from langchain_community.document_loaders import PlaywrightURLLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter, Language
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain_pinecone import PineconeVectorStore
from dotenv import load_dotenv
import os
import nltk
import ssl

# Ładowanie zmiennych środowiskowych z pliku .env
load_dotenv()

# Ustawienie kontekstu SSL bez weryfikacji certyfikatów
ssl._create_default_https_context = ssl._create_unverified_context

# Upewnij się, że punkt jest pobrany dla NLTK
nltk.download('punkt')

# Definiowanie funkcji przetwarzania dokumentów
def process_documents(loader):
    try:
        raw_documents = loader.load()
        print("LOADED successfully.")
        for doc in raw_documents:
            print(doc.page_content[:500])  # Wyświetl pierwsze 500 znaków każdego dokumentu
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return []
    print(f"loaded {len(raw_documents)} documents")
    
    text_splitter = RecursiveCharacterTextSplitter.from_language(chunk_size=1000, 
                                                   chunk_overlap=50, 
                                                   language=Language.HTML)
    documents = text_splitter.split_documents(documents=raw_documents)
    print(f"splitter {len(documents)} chunks")

    return documents

if __name__ == "__main__":
    print('advanced rag')

    # Definiowanie URL dokumentacji LangChain
    url = "https://api.python.langchain.com/en/latest/langchain_api_reference.html"

    # Wybór loadera i przetwarzanie dokumentów
    print("\n### Użycie Playwright URL Loader ###")
    try:
        urls = [url]
        loader = PlaywrightURLLoader(urls=urls, remove_selectors=["header", "footer"])
        print("Loader initialized successfully.")
        documents = process_documents(loader)
    except Exception as e:
        print(f"An error occurred: {str(e)}")

    # Użycie OpenAI do generowania embeddings
    embeddings = OpenAIEmbeddings(openai_api_key=os.getenv('OPENAI_API_KEY'))

    # Ładowanie do Pinecone
    print("\n### Ładowanie do Pinecone ###")
    PineconeVectorStore.from_documents(documents, 
                                       embeddings, 
                                       index_name=os.getenv('PINECONE_INDEX'))
    print("Documents successfully loaded into Pinecone.")
